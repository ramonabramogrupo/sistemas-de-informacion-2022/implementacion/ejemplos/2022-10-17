﻿/* 
  CONSULTAS DE COMBINACION INTERNAS
*/


-- 1	Nombre y edad de los ciclistas que han ganado etapas

  -- consulta correcta pero se puede mejorar
  SELECT 
      DISTINCT c.nombre,c.edad 
    FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal;
 
  -- la misma consulta con las tablas en otro orden
  -- resultado el mismo
  SELECT DISTINCT
    c.nombre,c.edad 
  FROM 
    etapa e JOIN ciclista c ON e.dorsal = c.dorsal;

  -- la misma consulta pero con using
  SELECT DISTINCT
    c.nombre,c.edad 
  FROM 
    etapa e JOIN ciclista c USING(dorsal);


  -- optimizar la consulta

  -- creamos una subconsulta
  -- C1
  -- dorsales de los ciclistas que han ganado etapas

  SELECT DISTINCT e.dorsal FROM etapa e;

  -- para terminar la consulta
  -- combino la tabla ciclista con C1

    SELECT DISTINCT 
        c.nombre, c.edad 
      FROM ciclista c JOIN
        (
          SELECT DISTINCT e.dorsal FROM etapa e
        ) c1 USING(dorsal);


  -- consulta completa con in
  -- VOY A EMULAR EL JOIN CON UN IN
  SELECT DISTINCT 
      c.nombre, c.edad 
    FROM ciclista c
      WHERE c.dorsal
        IN (SELECT DISTINCT e.dorsal FROM etapa e);


-- 2	Nombre y edad de los ciclistas que han ganado puertos
  
  SELECT DISTINCT 
      c.nombre, c.edad 
    FROM ciclista c JOIN puerto p ON c.dorsal = p.dorsal;
  
  -- LA MISMA SOLUCION PERO CON USING
  SELECT DISTINCT 
      c.nombre, c.edad 
    FROM ciclista c JOIN puerto p USING (dorsal);
   

  -- optimizar
  -- NECESITO EL DORSAL DE LOS CICLISTAS QUE HAN GANADO PUERTO
  -- C1

    SELECT DISTINCT p.dorsal FROM puerto p;

  -- PARA TERMINAR LA CONSULTA CON COMBINACION INTERNA
    SELECT DISTINCT 
        c.nombre, c.edad 
      FROM ciclista c JOIN 
        (
          SELECT DISTINCT p.dorsal FROM puerto p
        ) C1 USING(dorsal);

    -- LA CONSULTA ANTERIOR PODEMOS REALIZARLA CON WHERE IN 
    -- Y NO UTILIZAR JOIN
      SELECT DISTINCT 
          c.nombre,c.edad 
        FROM ciclista c 
        WHERE c.dorsal 
          IN (SELECT DISTINCT p.dorsal FROM puerto p);
        

-- 3	Nombre y edad de los ciclistas que han ganado etapas y puertos
  
  -- consulta sin optimizar
  SELECT DISTINCT 
      c.nombre, c.edad 
    FROM ciclista c 
      JOIN etapa e ON c.dorsal = e.dorsal
      JOIN puerto p ON c.dorsal = p.dorsal;

  -- consulta optimizada

    -- c1
    -- TODOS LOS CICLISTAS QUE HAN GANADO ETAPAS
    SELECT DISTINCT e.dorsal FROM etapa e;

    -- c2 
    -- TODOS LOS CICLISTAS QUE HAN GANADO PUERTOS
    SELECT DISTINCT p.dorsal FROM puerto p;
    
    
    -- c3
    -- ciclistas que han ganado etapas y puertos
    -- realizada con una combinacion interna
    SELECT 
        * 
      FROM 
      (SELECT DISTINCT e.dorsal FROM etapa e) C1
      JOIN 
      (SELECT DISTINCT p.dorsal FROM puerto p) C2
      USING(dorsal);


    -- c3
    -- SI REALIZO LA INTERSECCION DE LAS DOS 
    -- SUBCONSULTAS C1 Y C2
      SELECT DISTINCT e.dorsal FROM etapa e
        INTERSECT   
      SELECT DISTINCT p.dorsal FROM puerto p;

    -- COMO LA INTERSECCION NO FUNCION
    -- LA IMPLEMENTO CON UN NATURAL JOIN
    SELECT * FROM 
      (SELECT DISTINCT e.dorsal FROM etapa e) c1 
      NATURAL JOIN 
      (SELECT DISTINCT p.dorsal FROM puerto p) c2; 

    -- COMO LA INTERSECCION NO FUNCION
    -- LA IMPLEMENTO CON UN JOIN
    SELECT * FROM 
      (SELECT DISTINCT e.dorsal FROM etapa e) c1 
      JOIN 
      (SELECT DISTINCT p.dorsal FROM puerto p) c2
      USING(dorsal);

    -- COMO LA INTERSECCION NO FUNCION
    -- LA IMPLEMENTO CON UN WHERE
    
      SELECT 
          * 
        FROM (SELECT DISTINCT e.dorsal FROM etapa e) C1
        WHERE C1.dorsal IN (SELECT DISTINCT p.dorsal FROM puerto p);

    
    -- PARA TERMINAR LA CONSULTA
    -- TENGO QUE COMBINAR CICLISTAS CON C3
      SELECT DISTINCT 
          c.nombre,c.edad 
        FROM ciclista c JOIN
          (
            SELECT * FROM 
              (SELECT DISTINCT e.dorsal FROM etapa e) c1 
              NATURAL JOIN 
              (SELECT DISTINCT p.dorsal FROM puerto p) c2
          ) c3 USING(dorsal);


    -- PARA TERMINAR LA CONSULTA
    -- PUEDO UTILIZAR UN WHERE IN
      SELECT DISTINCT 
          c.nombre,c.dorsal 
        FROM ciclista c
          WHERE c.dorsal 
            IN (
                  SELECT * FROM 
                      (SELECT DISTINCT e.dorsal FROM etapa e) c1 
                      NATURAL JOIN 
                      (SELECT DISTINCT p.dorsal FROM puerto p) c2
                ); 
   

-- 4	Listar el director de los equipos que 
-- tengan ciclistas que hayan ganado alguna etapa

  -- SIN OPTIMIZAR
  SELECT DISTINCT 
      e.director 
    FROM equipo e 
      JOIN ciclista c ON e.nomequipo = c.nomequipo
      JOIN etapa e1 ON c.dorsal = e1.dorsal;

  
  -- OPTIMIZANDO LA CONSULTA
  
  -- E1
  -- CICLISTAS QUE HAN GANADO ETAPAS
    SELECT DISTINCT e.dorsal FROM etapa e;

SELECT * FROM equipo e;

  -- E2
  -- EQUIPOS QUE TIENEN CICLISTAS QUE HAN GANADO ETAPAS
    SELECT DISTINCT 
        c.nomequipo 
      FROM ciclista c JOIN 
        (
          SELECT DISTINCT e.dorsal FROM etapa e
        ) E1
        USING(dorsal);

-- TERMINO LA CONSULTA 
-- UTILIZANDO UNA COMBINACION INTERNA 
-- ENTRE E2 Y EQUIPOS
SELECT DISTINCT 
    e.director 
  FROM equipo e JOIN 
    (
      SELECT DISTINCT 
              c.nomequipo 
            FROM ciclista c JOIN 
              (
                SELECT DISTINCT e.dorsal FROM etapa e
              ) E1
              USING(dorsal)
    ) E2 USING(nomequipo);

-- TERMINO LA CONSULTA 
-- UTILIZANDO WHERE IN
  SELECT DISTINCT   
      e.director 
    FROM equipo e
      WHERE e.nomequipo 
        IN (
              SELECT DISTINCT 
                      c.nomequipo 
                    FROM ciclista c JOIN 
                      (
                        SELECT DISTINCT e.dorsal FROM etapa e
                      ) E1
                      USING(dorsal)
            );


-- 5	Dorsal y nombre de los ciclistas que hayan 
-- llevado algún maillot

  -- SIN OPTIMIZAR

  SELECT DISTINCT 
      c.dorsal, c.nombre 
    FROM ciclista c 
      JOIN lleva l ON c.dorsal = l.dorsal;

  -- PARA OPTIMIZAR LA CONSULTA

  -- c1
  -- DORSAL DE LOS CICLISTAS QUE HAN LLEVADO MAILLOTS
    SELECT DISTINCT l.dorsal FROM lleva l;
 

  -- PARA TERMINAR LA CONSULTA
  -- MEDIANTE UNA COMBINACION INTERNA
  SELECT 
      c.dorsal,c.nombre 
    FROM ciclista c JOIN 
      (
        SELECT DISTINCT l.dorsal FROM lleva l
      ) C1 USING(dorsal);
     

  -- PARA TERMINAR LA CONSULTA
  -- MEDIANTE UN WHERE IN
  SELECT 
      c.dorsal,c.nombre 
    FROM ciclista c    
      WHERE c.dorsal 
        IN (SELECT DISTINCT l.dorsal FROM lleva l);

-- 6	Dorsal y nombre de los ciclistas que hayan 
--    llevado el maillot amarillo
  
  -- consulta completa sin optimizar
  SELECT DISTINCT 
      c.dorsal,c.nombre 
    FROM ciclista c
      JOIN lleva l ON c.dorsal = l.dorsal
      JOIN maillot m ON l.código = m.código
    WHERE 
      m.color="amarillo";



  -- OPTIMIZAR CONSULTA
  -- c1: codigo del maillot amarillo
  SELECT m.código FROM maillot m WHERE m.color="amarillo";

  -- c2
  -- DORSAL DE LOS CICLISTAS QUE HAN LLEVADO MAILLOT AMARILLO 
    SELECT DISTINCT  
        l.dorsal 
      FROM lleva l JOIN 
        (
          SELECT m.código FROM maillot m WHERE m.color="amarillo"
        ) c1 USING(código);

   -- C2
   -- PODEMOS IMPLEMENTAR EL C2 CON WHERE
    SELECT DISTINCT 
        l.dorsal 
      FROM lleva l 
        WHERE l.código 
          IN (SELECT m.código FROM maillot m WHERE m.color="amarillo");


-- CONSULTA FINAL
-- COMBINANDO LA CONSULTA C2 CON CICLISTAS
  SELECT 
      c.dorsal, c.nombre 
    FROM ciclista c JOIN 
      (
        SELECT DISTINCT  
                l.dorsal 
              FROM lleva l JOIN 
                (
                  SELECT m.código FROM maillot m WHERE m.color="amarillo"
                ) c1 USING(código)
      ) C2  USING (dorsal);

-- TERMINAMOS LA CONSULTA
-- AHORA UTILIZAMOS WHERE IN
  SELECT 
      c.dorsal, c.nombre 
    FROM ciclista c
    WHERE c.dorsal 
      IN (
           SELECT DISTINCT  
                l.dorsal 
              FROM lleva l JOIN 
                (
                  SELECT m.código FROM maillot m WHERE m.color="amarillo"
                ) c1 USING(código)
          );


  

-- PODIAMOS HABER OPTIMIZADO UN POCO MAS LA CONSULTA

-- c1: codigo del maillot amarillo
SELECT m.código FROM maillot m WHERE m.color="amarillo";

-- llevaOptimizado
-- QUITO LOS REPETIDOS EN LA TABLA LLEVA 
-- POR ETAPA
  SELECT DISTINCT l.dorsal,l.código FROM lleva l;


  -- c2
  -- DORSAL DE LOS CICLISTAS QUE HAN LLEVADO MAILLOT AMARILLO 
    SELECT DISTINCT  
        llevaOptimizado.dorsal
      FROM 
        (
          SELECT DISTINCT l.dorsal,l.código FROM lleva l
        ) llevaOptimizado
      JOIN 
        (
          SELECT m.código FROM maillot m WHERE m.color="amarillo"
        ) c1 USING(código);

-- CONSULTA FINAL
-- COMBINO C2 CON CICLISTA
SELECT 
    c.dorsal, c.nombre 
  FROM ciclista c JOIN 
    (
      SELECT DISTINCT  
              llevaOptimizado.dorsal
            FROM 
              (
                SELECT DISTINCT l.dorsal,l.código FROM lleva l
              ) llevaOptimizado
            JOIN 
              (
                SELECT m.código FROM maillot m WHERE m.color="amarillo"
              ) c1 USING(código)    
    ) C2 USING(dorsal);
    

-- 7	Dorsal de los ciclistas que hayan llevado algún maillot 
--    y que han ganado etapas

SELECT DISTINCT 
    e.dorsal 
  FROM lleva l JOIN etapa e ON l.dorsal = e.dorsal; 


-- OPTIMIZAMOS LA CONSULTA

-- C1
-- DORSAL DE LOS CICLISTAS QUE HAN GANADO ETAPAS
 
  SELECT DISTINCT e.dorsal FROM etapa e; 

-- C2
-- CICLISTAS QUE HAN LLEVADO ALGUN MAILLOT
  SELECT DISTINCT l.dorsal FROM lleva l;  

-- TERMINAR LA CONSULTA
-- INTERSECCIONAR C1 CON C2

SELECT DISTINCT e.dorsal FROM etapa e
  INTERSECT 
SELECT DISTINCT l.dorsal FROM lleva l;  


-- COMO LA INTERSECCION NO LA TENGO
-- LA REALIZO CON UN JOIN
SELECT DISTINCT  
    C1.dorsal
  FROM 
    (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) C1
    JOIN
    (
      SELECT DISTINCT l.dorsal FROM lleva l    
    ) C2 USING(dorsal);
  
-- COMO LA INTERSECCION NO LA TENGO
-- LA REALIZO CON UN NATURAL JOIN
SELECT DISTINCT  
    C1.dorsal
  FROM 
    (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) C1
    NATURAL JOIN
    (
      SELECT DISTINCT l.dorsal FROM lleva l    
    ) C2;

-- COMO LA INTERSECCION NO LA TENGO
-- LA REALIZO CON UN WHERE IN

SELECT  
    C1.dorsal 
  FROM 
    (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) C1
  WHERE C1.dorsal
    IN (SELECT DISTINCT l.dorsal FROM lleva l);
  

 -- 8	Indicar el numetapa y los km de las etapas que tengan puertos

SELECT 
    DISTINCT p.numetapa, e1.kms 
  FROM puerto p JOIN etapa e1 ON p.numetapa = e1.numetapa;

-- OPTIMIZAMOS LA CONSULTA

-- C1
-- NUMETAPA DE LAS ETAPAS QUE TIENEN PUERTO

  SELECT DISTINCT p.numetapa FROM puerto p; 

-- TERMINAR LA CONSULTA
-- COMBINO LA TABLA ETAPA CON C1
  SELECT 
      e.numetapa,e.kms 
    FROM etapa e JOIN 
      (
        SELECT DISTINCT p.numetapa FROM puerto p
      ) C1 USING(numetapa);

-- 9	Indicar los km de las etapas que hayan ganado ciclistas del Banesto 
--    y que tengan puertos

 SELECT DISTINCT 
      e.kms 
  FROM etapa e 
    JOIN ciclista c ON e.dorsal = c.dorsal 
    JOIN puerto p ON e.numetapa = p.numetapa
  WHERE
    c.nomequipo="banesto";

-- c1
-- dorsal de los ciclista de banesto
SELECT 
    DISTINCT c.dorsal 
  FROM ciclista c 
  WHERE c.nomequipo="banesto";


-- c2
-- etapas que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- TERMINAR LA CONSULTA
-- REALIZAR UNA COMBINACION ENTRE ETAPAS, C1 Y C2
SELECT DISTINCT  
    e.kms 
  FROM etapa e 
    JOIN 
      (
        SELECT 
            DISTINCT c.dorsal 
          FROM ciclista c 
          WHERE c.nomequipo="banesto"
      ) C1 USING(dorsal)
    JOIN 
      (
        SELECT DISTINCT p.numetapa FROM puerto p      
      ) C2 USING(numetapa);

-- 10	Listar el número de ciclistas que hayan ganado alguna etapa con puerto

-- MOSTRAR EL DORSAL
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

-- CONTAR
SELECT 
    COUNT(DISTINCT e.dorsal) AS numero
  FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;


-- OPTIMIZAR LA CONSULTA

-- C1
-- ETAPAS QUE TIENEN PUERTO
SELECT DISTINCT p.numetapa FROM puerto p;

-- TERMINO LA CONSULTA
-- COMBINANDO LA TABLA ETAPA CON C1
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e JOIN 
    (
      SELECT DISTINCT p.numetapa FROM puerto p      
    ) C1 USING(numetapa);

-- TERMINO LA CONSULTA
-- CON WHERE IN
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e
  WHERE e.numetapa 
    IN (
          SELECT DISTINCT p.numetapa FROM puerto p          
       );

-- SI QUEREMOS SABER CUANTOS Y NO QUIENES

-- COMBINANDO LA TABLA ETAPA CON C1
SELECT 
    COUNT(DISTINCT e.dorsal) AS numero 
  FROM etapa e JOIN 
    (
      SELECT DISTINCT p.numetapa FROM puerto p      
    ) C1 USING(numetapa);

-- CON WHERE IN
SELECT 
    COUNT(DISTINCT e.dorsal) AS numero 
  FROM etapa e
  WHERE e.numetapa 
    IN (
          SELECT DISTINCT p.numetapa FROM puerto p          
       );


-- 11	Indicar el nombre de los puertos que hayan sido ganados 
--    por ciclistas de Banesto

  SELECT 
      p.nompuerto 
    FROM puerto p 
      JOIN ciclista c ON p.dorsal = c.dorsal
    WHERE c.nomequipo="banesto";

  -- optimizamos la consulta
 
  -- c1 
  -- DORSAL DE LOS CICLISTAS DE BANESTO
  SELECT 
      c.dorsal 
    FROM ciclista c 
    WHERE c.nomequipo="banesto";

  -- TERMINAR LA CONSULTA
  -- COMBINAR PUERTO CON C1
  SELECT 
      p.nompuerto 
    FROM puerto p JOIN 
      (
        SELECT 
              c.dorsal 
            FROM ciclista c 
            WHERE c.nomequipo="banesto"
      ) C1 USING(dorsal);

  -- TERMINAR LA CONSULTA
  -- UTILIZANDO UN WHERE IN
  SELECT 
      p.nompuerto 
    FROM puerto p
    WHERE p.dorsal IN 
      (
        SELECT 
            c.dorsal 
          FROM ciclista c 
          WHERE c.nomequipo="banesto"
      );


  
-- 12	Listar el número de etapas que tengan puerto que 
--    hayan sido ganados por ciclistas de Banesto con mas de 200 km

 SELECT 
      DISTINCT e.numetapa
    FROM etapa e
      JOIN puerto p ON e.numetapa = p.numetapa
      JOIN ciclista c ON e.dorsal = c.dorsal
    WHERE 
      c.nomequipo="banesto"
      AND 
      e.kms>=200;

-- OPTIMIZAR LAS CONSULTAS
-- TENGO QUE INTENTAR REALIZAR LOS WHERE ANTES DE LOS JOINS


-- C1
-- DORSAL DE LOS CICLISTAS DE BANESTO
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo="banesto";

-- C2
-- ETAPAS QUE TENGAN MAS DE 200 KM
SELECT 
    e.numetapa, 
    e.dorsal 
  FROM etapa e 
  WHERE e.kms>=200;

-- C3
-- ETAPAS CON PUERTO
SELECT DISTINCT p.numetapa FROM puerto p;

-- CONSULTA FINAL
-- COMBINAR C1, C2 Y C3
SELECT 
    DISTINCT C2.numetapa
  FROM 
      (
        SELECT c.dorsal FROM ciclista c WHERE c.nomequipo="banesto"
      ) C1 
    JOIN 
      (
        SELECT 
            e.numetapa, 
            e.dorsal 
          FROM etapa e 
          WHERE e.kms>=200      
      ) C2 USING(dorsal)
    JOIN 
      (
        SELECT DISTINCT p.numetapa FROM puerto p      
      ) c3 USING(numetapa);


-- PARA TERMINAR LA CONSULTA
 SELECT 
    DISTINCT c2.numetapa
  FROM 
      (
        SELECT 
            e.numetapa, 
            e.dorsal 
          FROM etapa e 
          WHERE e.kms>=200
      ) C2
    JOIN 
      (
        SELECT DISTINCT p.numetapa FROM puerto p
      ) C3 USING (numetapa)
  WHERE c2.dorsal IN
    (
       SELECT c.dorsal FROM ciclista c WHERE c.nomequipo="banesto"
    );

-- PARA TERMINAR LA CONSULTA
SELECT 
    DISTINCT numetapa  
  FROM 
    (
       SELECT 
            e.numetapa, 
            e.dorsal 
          FROM etapa e 
          WHERE e.kms>=200
    ) C2
  WHERE 
      dorsal IN 
        (
          SELECT c.dorsal FROM ciclista c WHERE c.nomequipo="banesto"
        )
    AND 
      numetapa IN 
        (
          SELECT DISTINCT p.numetapa FROM puerto p
        );
    







